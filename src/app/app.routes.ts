import { Routes } from "@angular/router";

import { Dashboard1Component } from "./views/dashboards/dashboard1.component";
import { Dashboard2Component } from "./views/dashboards/dashboard2.component";
import { Dashboard3Component } from "./views/dashboards/dashboard3.component";
import { Dashboard4Component } from "./views/dashboards/dashboard4.component";
import { Dashboard41Component } from "./views/dashboards/dashboard41.component";
import { Dashboard5Component } from "./views/dashboards/dashboard5.component";

import { StarterViewComponent } from "./views/appviews/starterview.component";
import { LoginComponent } from "./views/appviews/login.component";

import { BlankLayoutComponent } from "./components/common/layouts/blankLayout.component";
import { BasicLayoutComponent } from "./components/common/layouts/basicLayout.component";
import { TopNavigationLayoutComponent } from "./components/common/layouts/topNavigationLayout.component";
import { AuthGuard } from './_helpers';

import { UpcomingEventsComponent } from './views/upcoming-events/upcoming-events.component';
import { EventPlannerListComponent } from './views/event-planner-list/event-planner-list.component';
import { EventsPlannerDetailsComponent } from './views/events-planner-details/events-planner-details.component';
import { VendorListingComponent } from './views/vendor-listing/vendor-listing.component';
import { VendorDetailsComponent } from './views/vendor-details/vendor-details.component';
import { EventsListComponent } from './views/events-list/events-list.component';
import { EventDetailsComponent } from './views/event-details/event-details.component';
import { ServiceManagementComponent } from './views/service-management/service-management.component';
import { EventTypeComponent } from './views/event-type/event-type.component';
import { TransactionsComponent } from './views/transactions/transactions.component';
import { EventPlannerDetailsComponent } from './views/event-planner-details/event-planner-details.component';
export const ROUTES: Routes = [
  // Main redirect
  // { path: '', redirectTo: 'dashboard', pathMatch: 'full' },

  // App views
  {
    path: '', component: BasicLayoutComponent,
    children: [
      { path: 'dashboard', component: Dashboard2Component },
      { path: 'event-planners', component: EventPlannerListComponent },
      { path: 'vendors', component: VendorListingComponent },
      { path: 'events', component: EventsListComponent },
      { path: 'service-management', component: ServiceManagementComponent },
      { path: 'event-planner-details', component: EventsPlannerDetailsComponent },
      { path: 'vendor-details', component: VendorDetailsComponent },
      { path: 'event-details', component: EventDetailsComponent },
      { path: 'event-type', component: EventTypeComponent },
      { path: 'transactions', component: TransactionsComponent },
      { path: 'planner-details', component: EventPlannerDetailsComponent },
    ],
    canActivate: [AuthGuard] 
  },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'event-planners', component: EventPlannerListComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'vendors', component: VendorListingComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'events', component: EventsListComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'service-management', component: ServiceManagementComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'event-planner-details', component: EventsPlannerDetailsComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'vendor-details', component: VendorDetailsComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'event-details', component: EventDetailsComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'event-type', component: EventTypeComponent }
  //   ]
  // },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'transactions', component: TransactionsComponent }
  //   ]
  // },
  {
    path: '', component: BlankLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: '', component: LoginComponent },
    ]
  },
  // {
  //   path: '', component: BasicLayoutComponent,
  //   children: [
  //     { path: 'planner-details', component: EventPlannerDetailsComponent },
  //   ]
  // },
  

  // Handle all other routes
  { path: '**', redirectTo: 'login' }
];
