import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private resourceUrl = `${environment.baseUrl}`;
  constructor(private http: HttpClient) { }

  getDashboardData() {
    return this.http.get<any>(`${this.resourceUrl}/user/dashboard`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  eventList(service_id: number = null) {
    return this.http.get<any>(`${this.resourceUrl}/event/listing?service_id=${service_id}`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  userList(params:any) {
    
    return this.http.get<any>(`${this.resourceUrl}/user/listing`, {params:params, observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  userDetails(id: number) {
    return this.http.get<any>(`${this.resourceUrl}/user?id=${id}`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  serviceList(params) {
    return this.http.get<any>(`${this.resourceUrl}/service/listing`, {params:params, observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  eventTypeList() {
    return this.http.get<any>(`${this.resourceUrl}/event/types`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
  paymentList() {
    return this.http.get<any>(`${this.resourceUrl}/payment/listing`, { observe: 'response' })
      .pipe(map(res => {
        return res.body;
      }));
  }
}
