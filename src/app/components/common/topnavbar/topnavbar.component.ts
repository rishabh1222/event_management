import { Component } from '@angular/core';
import { smoothlyMenu } from '../../../app.helpers';
declare var jQuery:any;
import { AuthenticationService } from '../../../_services/authentication.service';
@Component({
  selector: 'topnavbar',
  templateUrl: 'topnavbar.template.html'
})
export class TopNavbarComponent {
  constructor( private authenticationService: AuthenticationService) {}
  toggleNavigation(): void {
    jQuery("body").toggleClass("mini-navbar");
    smoothlyMenu();
  }
  logout(){
    this.authenticationService.logout()
  }
}
