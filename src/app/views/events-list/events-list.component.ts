import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiService } from '../../_services/api.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit {
  constructor( private apiService: ApiService) { }
  public eventsList:any;
  ngOnInit() {
    this.getEventsList()
  }
  getEventsList(){
    
    this.apiService.eventList()
      .pipe(first())
      .subscribe(
        data => {
          console.table(data)
          this.eventsList = data
         
        },
        error => {
          console.warn(error)
         
        });

  }

}
