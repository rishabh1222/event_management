import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiService } from '../../_services/api.service';
@Component({
  selector: 'app-event-type',
  templateUrl: './event-type.component.html',
  styleUrls: ['./event-type.component.css']
})
export class EventTypeComponent implements OnInit {


  constructor( private apiService: ApiService) { }
  public eventTypeList:any;
  public request: any = {
    page: 1,
    limit: 20
  };
  ngOnInit() {
    this.getEventTypeList()
  }
  getEventTypeList(){
    
    this.apiService.eventTypeList()
      .pipe(first())
      .subscribe(
        data => {
          console.table(data)
          this.eventTypeList = data
         
        },
        error => {
          console.warn(error)
         
        });

  }

}
