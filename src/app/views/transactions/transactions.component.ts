import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiService } from '../../_services/api.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  constructor( private apiService: ApiService) { }
  public transactionList:any;

  ngOnInit() {
    this.getTransactionList()
  }
  getTransactionList(){
    
    this.apiService.paymentList()
      .pipe(first())
      .subscribe(
        data => {
          console.table(data)
          this.transactionList = data
         
        },
        error => {
          console.warn(error)
         
        });

  }


}
