import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiService } from '../../_services/api.service';

@Component({
  selector: 'app-vendor-listing',
  templateUrl: './vendor-listing.component.html',
  styleUrls: ['./vendor-listing.component.css']
})
export class VendorListingComponent implements OnInit {

  constructor(private apiService: ApiService) { }
  public vendorsList:any = null
  public request:any = {
    phone_number:"",
    email:"",
    name:"",
    page:1,
    limit:20
  };
  ngOnInit() {
    this.getVendorsList()
  }
  getVendorsList(){
    let request = {
      user_type:2,
      page:this.request.page-1,
      limit:this.request.limit,
      name:this.request.name,
      email:this.request.email,
      phone_number:this.request.phone_number
    }
    this.apiService.userList(request)
      .pipe(first())
      .subscribe(
        data => {
          console.table(data)
          this.vendorsList = data
         
        },
        error => {
          console.warn(error)
         
        });

  }

}
