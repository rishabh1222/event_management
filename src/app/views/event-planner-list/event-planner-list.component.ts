import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiService } from '../../_services/api.service';
@Component({
  selector: 'app-event-planner-list',
  templateUrl: './event-planner-list.component.html',
  styleUrls: ['./event-planner-list.component.css']
})
export class EventPlannerListComponent implements OnInit {

  constructor( private apiService: ApiService) { }
  public eventPlannerList:any;
  public request:any = {
    phone_number:"",
    email:"",
    name:"",
    page:1,
    limit:20
  };
  ngOnInit() {
    this.getEventPlannerList()
  }
  getEventPlannerList(){
    let request = {
      user_type:1,
      page:this.request.page-1,
      limit:this.request.limit,
      name:this.request.name,
      email:this.request.email,
      phone_number:this.request.phone_number
    }
    this.apiService.userList(request)
      .pipe(first())
      .subscribe(
        data => {
          console.table(data)
          this.eventPlannerList = data
         
        },
        error => {
          console.warn(error)
         
        });

  }

}
