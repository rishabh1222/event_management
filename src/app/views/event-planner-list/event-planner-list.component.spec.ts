import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPlannerListComponent } from './event-planner-list.component';

describe('EventPlannerListComponent', () => {
  let component: EventPlannerListComponent;
  let fixture: ComponentFixture<EventPlannerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventPlannerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPlannerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
