import { Component, OnInit,AfterViewInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiService } from '../../_services/api.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {EditDialogueComponent} from './edit-dialogue/edit-dialogue.component'
import { NgxSmartModalService } from 'ngx-smart-modal';
@Component({
  selector: 'app-service-management',
  templateUrl: './service-management.component.html',
  styleUrls: ['./service-management.component.css']
})
export class ServiceManagementComponent implements OnInit, AfterViewInit {

  constructor(private apiService: ApiService,public ngxSmartModalService: NgxSmartModalService,
    private modalService: NgbModal) { }
  public serviceList: any;
  public request: any = {
    keywords: "",
    page: 1,
    limit: 20
  };
  ngOnInit() {
    this.getServiceList()
  }
  
  ngAfterViewInit() {
    const pen: Object = {
      prop1: 'test',
      prop2: true,
      prop3: [{ a: 'a', b: 'b' }, { c: 'c', d: 'd' }],
      prop4: 327652175423
    };
    this.ngxSmartModalService.setModalData(pen, 'popupOne');
  }
  open() {
    this.ngxSmartModalService.getModal('popupOne').open()
  }
  getServiceList() {
    let request = {
      keyword:this.request.keywords,
      page:this.request.page-1,
      limit:this.request.limit,
      name:this.request.name,
      email:this.request.email,
      phone_number:this.request.phone_number
    }
    this.apiService.serviceList(request)
      .pipe(first())
      .subscribe(
        data => {
          console.table(data)
          this.serviceList = data

        },
        error => {
          console.warn(error)

        });

  }

}
