import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventPlannerDetailsComponent } from './event-planner-details.component';

describe('EventPlannerDetailsComponent', () => {
  let component: EventPlannerDetailsComponent;
  let fixture: ComponentFixture<EventPlannerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventPlannerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventPlannerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
