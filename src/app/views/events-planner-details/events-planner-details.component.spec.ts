import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsPlannerDetailsComponent } from './events-planner-details.component';

describe('EventsPlannerDetailsComponent', () => {
  let component: EventsPlannerDetailsComponent;
  let fixture: ComponentFixture<EventsPlannerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsPlannerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsPlannerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
