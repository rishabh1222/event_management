import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {RouterModule} from "@angular/router";
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {NgbModule,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import {ROUTES} from "./app.routes";
import { AppComponent } from './app.component';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { NgxSmartModalModule,NgxSmartModalService } from 'ngx-smart-modal';
// App views

import {DashboardsModule} from "./views/dashboards/dashboards.module";
import {AppviewsModule} from "./views/appviews/appviews.module";
// App modules/components
import {LayoutsModule} from "./components/common/layouts/layouts.module";
import { UpcomingEventsComponent } from './views/upcoming-events/upcoming-events.component';
import { EventPlannerListComponent } from './views/event-planner-list/event-planner-list.component';
import { EventsPlannerDetailsComponent } from './views/events-planner-details/events-planner-details.component';
import { VendorListingComponent } from './views/vendor-listing/vendor-listing.component';
import { VendorDetailsComponent } from './views/vendor-details/vendor-details.component';
import { EventsListComponent } from './views/events-list/events-list.component';
import { EventDetailsComponent } from './views/event-details/event-details.component';
import { ServiceManagementComponent } from './views/service-management/service-management.component';
import { EventsListingComponent } from './components/common/events-listing/events-listing.component';
import { TransactionsComponent } from './views/transactions/transactions.component';
import { EventTypeComponent } from './views/event-type/event-type.component';
import { EventPlannerDetailsComponent } from './views/event-planner-details/event-planner-details.component';
import { EditDialogueComponent } from './views/service-management/edit-dialogue/edit-dialogue.component';


@NgModule({
  declarations: [
    AppComponent,
    UpcomingEventsComponent,
    EventPlannerListComponent,
    EventsPlannerDetailsComponent,
    VendorListingComponent,
    VendorDetailsComponent,
    EventsListComponent,
    EventDetailsComponent,
    ServiceManagementComponent,
    EventsListingComponent,
    TransactionsComponent,
    EventTypeComponent,
    EventPlannerDetailsComponent,
    EditDialogueComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    DashboardsModule,
    LayoutsModule,
    AppviewsModule,
    RouterModule.forRoot(ROUTES),
    ReactiveFormsModule,
    NgxSmartModalModule.forRoot()
  ],
  // providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  providers: [   
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    NgxSmartModalService
  ],
  entryComponents: [EditDialogueComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
