export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    session_id?: string;
}